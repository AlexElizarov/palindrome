using System;

namespace Domain
{
    public class PalindromeDetector
    {
        private readonly int defaultValue = 1;

        public int Detect(int input)
        {
            for (var number = input-1; number > 0; number--)
            {
                if (IsPrime(number) && IsPalindrome(number))
                {
                    return number;
                }
            }
            return defaultValue;
        }

        private bool IsPrime(int number)
        {
            for (int i = 2; i <= Math.Sqrt(number); i++)
            {
                if (number%i == 0)
                {
                    return false;
                }
            }
            return true;
        }

        private bool IsPalindrome(int number)
        {
            string temp = number.ToString();
            for (int i = 0, j = (temp.Length - 1); i < temp.Length / 2; i++, j--)
            {
                if (!temp[i].Equals(temp[j]))
                {
                    return false;
                }
            }
            return true;
        }
    }
}