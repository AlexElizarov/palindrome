﻿#region Usings

using Domain;
using NUnit.Framework;

#endregion

namespace PolindromTests
{
    [TestFixture]
    internal class WhenDeterminePalindrome
    {
        [Test]
        public void ReturnSingleNumberIfInputIsThatNumber()
        {
            var palindromeDetector = new PalindromeDetector();

            var result = palindromeDetector.Detect(2);

            Assert.AreEqual(1, result);
        }

        [Test]
        public void ReturnsMaxOneDigitPrimePalindrome()
        {
            var palindromeDetector = new PalindromeDetector();

            var result = palindromeDetector.Detect(10);

            Assert.AreEqual(7, result);
        }

        [Test]
        public void ReturnsMaxPalindromeIfInputIsRandomOneDigitNumber()
        {
            var palindromeDetector = new PalindromeDetector();

            var result = palindromeDetector.Detect(4);

            Assert.AreEqual(3, result);
        }

        [Test]
        public void ReturnMaxPalindromeForThreeDigitInput()
        {
            var palindromeDetector = new PalindromeDetector();

            var result = palindromeDetector.Detect(1000);

            Assert.AreEqual(929, result);
        }

        [Test]
        public void ReturnDefaultValueForInput0()
        {
            var palindromeDetector = new PalindromeDetector();

            var result = palindromeDetector.Detect(0);

            Assert.AreEqual(1, result);
        }

        [Test]
        public void ReturnDefaultValueForInputNegative()
        {
            var palindromeDetector = new PalindromeDetector();

            var result = palindromeDetector.Detect(-1);

            Assert.AreEqual(1, result);
        }

        [Test]
        public void Return373WhenInput500()
        {
            var palindromeDetector = new PalindromeDetector();

            var result = palindromeDetector.Detect(500);

            Assert.AreEqual(383, result);
        }
    }
}